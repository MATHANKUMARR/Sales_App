package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.Entity.CustomerLogin;
import com.example.demo.Entity.Sales;
import com.example.demo.repo.SalesRepo;  


@SpringBootApplication
public class SalesAppApplication implements CommandLineRunner{

	@Autowired
	private SalesRepo repo;	
	
	public static void main(String[] args) {
		SpringApplication.run(SalesAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		Sales s1=new Sales("Soap","12");
		Sales s2=new Sales("Water","10");
		Sales s3=new Sales("Garam Masala","38");
		Sales s4=new Sales("5 Star","5");
		Sales s5=new Sales("Maggi","20");
		
		repo.save(s1);
		repo.save(s2);
		repo.save(s3);
		repo.save(s4);
		repo.save(s5);
		
	}

}
