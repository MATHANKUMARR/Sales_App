package com.example.demo.Exception;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Configuration.Config;
import com.example.demo.Entity.CustomerLogin;

@ControllerAdvice
public class LoginException {
	
	//Autowired with Configuration class
	@Autowired
	private Config config;
	
	//Exception for SQLException
	@ExceptionHandler(SQLException.class)
	public String handleSQLException(SQLException sqlException) {
		return "DatabaseError";
	}
	
	//User define Exception for Customer Login
	@ExceptionHandler(CustomerValidationException.class)
	public ModelAndView handleCustomerValidationException(CustomerValidationException customerValidationException) {
		
		//Get the Error message from Configuration class
		String LoginError = config.getProperty("LoginError");
		ModelAndView mv = new ModelAndView("customerLogin");
		
		//Error message send to the HTML page
		mv.addObject("error", LoginError);
		return mv;
	}
	
	//User define Exception for Admin Login
	@ExceptionHandler(AdminValidationException.class)
	public ModelAndView handleAdminValidationException(AdminValidationException adminValidationException, Model model) {
		
		//Get the Error message from Configuration class
		String LoginError = config.getProperty("LoginError");
		ModelAndView mv = new ModelAndView("register");
		CustomerLogin customer = new CustomerLogin();
		model.addAttribute("customer",customer);
		
		//Error message send to the HTML page
		mv.addObject("error", LoginError);
		return mv;
	}
}
