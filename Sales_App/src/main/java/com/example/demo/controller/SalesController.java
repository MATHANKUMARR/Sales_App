package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.Entity.CustomerLogin;
import com.example.demo.Entity.Sales;
import com.example.demo.Exception.AdminValidationException;
import com.example.demo.Exception.CustomerValidationException;
import com.example.demo.service.AllService;

@Controller
public class SalesController {
	
	//Auto wired of Service
	@Autowired
	AllService service;
	
	//Front page of the application
	@GetMapping("/")
	public String indexPage() {
		return "index";
	}
	
	@GetMapping("/login")
	public String loginPage(Model model) {
		service.FirstPage(model);   //call service class method
		return "register";
	}	
	
	@GetMapping("/customer_home")
	public String customer_home(Model model) {
		service.customerHomePage(model);       //model passed to the service class
		return "customer_home";
	}
	
	@GetMapping("/owner_home")
	public String homePage(Model model) {
		service.ownerHomePage(model);
		return "owner_home";
	}
	
	@GetMapping("/customerLogin")
	public String customerLogin() {
	    return "customerLogin";	
	}
	
	@PostMapping("/saveCustomer")
	public String saveCustomer(@ModelAttribute("customer") CustomerLogin customerLogin) {
		service.saveCustomerDetails(customerLogin);
		
		//Redirecting the page
		return "redirect:/customerLogin";	
	}
	
	//Model - current HTML page
	@GetMapping("/saveSalesPage")
	public String saveSalesPage(Model model) {
		service.saveSalesPage(model);
		return "add_sales";	
	}
	
	@PostMapping("/saveSales")
	public String saveSsales(@ModelAttribute("sales") Sales sales) {
		service.saveSales(sales);
		return "redirect:/owner_home";
	}
	
	//Passing value to the service class
	@RequestMapping("/updateSalesPage/{id}")	
	public String showUpdateStudentPage(@PathVariable("id") int id, Model model) {
		service.updateSales(id, model);
		return "update_sales";
	}
	
	@GetMapping("/deleteSales/{id}")
	public String deleteStudent(@PathVariable("id") int id) {
		service.deleteSales(id);   //Default repository method - delete by id
		return "redirect:/owner_home";
	}
	
	@RequestMapping("/customer_check")
	public String customerLogin(HttpServletRequest request,Model model) throws CustomerValidationException{  //Exception call
		String customerLoginResult = service.customerCheck(request, model);
		return customerLoginResult;
	}
	
	@RequestMapping("/owner_check")
	public String ownerLogin(HttpServletRequest request,Model model) throws AdminValidationException {
		String ownerLoginResult = service.ownerCheck(request, model);
		return ownerLoginResult;
	}
	
	@RequestMapping("/add_cart/{id}")
	public String cart(@PathVariable("id") int id, Model model) {
		service.addToCart(id,model);
		//System.out.println(id);
		return "add_success";
	}
	
	@RequestMapping("/mycart")
	public String mycart(Model model) {
		service.mycart(model);
		return "cart";
	}
	
	@RequestMapping("/customerLogout")
	public String customerLogout() {
		return "customerLogout";
	}
	
	@RequestMapping("/remove_cart/{id}")
	public String removeFromCart(@PathVariable("id") int id, Model model) {
		service.removeItem(id,model);
		return "redirect:/mycart";
	}
	
	@RequestMapping("/calculate_bill")
	public String calculate_bill(HttpServletRequest request,Model model) {
		return "recipt";
	}
	
	@RequestMapping("/pay")
	public String payment() {
		return "payment";
	}
	
	@RequestMapping("/adminLogout")
	public String adminLogout() {
		return "adminLogout";
	}
	
	@RequestMapping("/payment_process")
	public String payment_process() {
		return "payment_process";
	}
	
	@RequestMapping("/payment_success")
	public String payment_success() {
		return "payment_success";
	}
	
	@RequestMapping("/goToHome")
	public String goToHome() {
		service.emptyCart();  //Delete all items from cart
		return "redirect:/customer_home";
	}
	
	@RequestMapping("/files")
	public String goToHe() {
		return "files";
	}
}
