package com.example.demo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.example.demo.Entity.CustomerLogin;
import com.example.demo.Entity.Sales;
import com.example.demo.Exception.AdminValidationException;
import com.example.demo.Exception.CustomerValidationException;
import com.example.demo.repo.CustomerRepo;
import com.example.demo.repo.SalesRepo;

@Component
public class AllService {
	
	//Sales entity class repository autowired
	@Autowired
	private SalesRepo repo;
	
	//Customer details entity class repository autowired
	@Autowired
	private CustomerRepo repo1;
	
	//Cart for customer
	List<Sales> cart = new ArrayList<Sales>();  
	
	//Front page
	public void FirstPage(Model model) {
		CustomerLogin customer = new CustomerLogin(); //Send the object to the HTML page
		model.addAttribute("customer",customer);
	}
	
	//Customer home page with all product details
	public void customerHomePage(Model model) {
		List<Sales> saleslist = (List<Sales>) repo.findAll(); //find all entities in a table
		model.addAttribute("saleslist",saleslist);
	}
	
	//Admin home page with all product details
	public void ownerHomePage(Model model) {
		List<Sales> saleslist = (List<Sales>) repo.findAll();
		model.addAttribute("saleslist",saleslist);
	}
	
	//store new product into the database
	public void saveCustomerDetails(CustomerLogin customerLogin) {
		repo1.save(customerLogin);  //Save the details into the table
	}
	
	//New Sales
	public void saveSalesPage(Model model) {
		Sales sales = new Sales();
		model.addAttribute("sales",sales); //Send to the HTML page
	}
	
	public void saveSales(Sales sales) {
		repo.save(sales);
	}
	
	//Update Product Details
	public void updateSales(int id,Model model) {
		Optional<Sales> temp=repo.findById(id);  //Find using id value and store it in list
		Sales sales=temp.get();
		model.addAttribute("sales",sales);
	}
	
	//Delete product into the table
	public void deleteSales(int id) {
		repo.deleteById(id);
	}
	
	//Customer UserName/Password checking
	public String customerCheck(HttpServletRequest request,Model model) throws CustomerValidationException {  //Exception
		
		//Get username/password in HTML page and store it in the String
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		
		List<CustomerLogin> customers = (List<CustomerLogin>) repo1.findAll(); //Get all entities in list and store it into list
		for(int i=0;i<customers.size();i++) {
			//Check if the username and passoword match
			if(userName.equalsIgnoreCase(customers.get(i).getUserName()) && password.equals(customers.get(i).getPassword())) {
				//Return the customer success
				return "customer_success";
			}
		}
			//Exception call
		throw new CustomerValidationException();
	}
	
	//Admin passowrd and Id checking
	public String ownerCheck(HttpServletRequest request,Model model) throws AdminValidationException {  //Exception
		String Id = request.getParameter("username");
		String pass = request.getParameter("password");
			//Check if it is matched or not
			if(Id.equalsIgnoreCase("Mathankumar R") && pass.equals("mathan1234")) {
				return "owner_success";
			}
			
			//Else Exception will throw
		throw new AdminValidationException();
	}
	
	//Add item to the cart
	public void addToCart(int id,Model model) {
		Optional<Sales> temp = repo.findById(id);
		Sales sales = temp.get();
		
		//Add to the cart List
		cart.add(sales);
	}

	//Return the Full cart values
	public void mycart(Model model) {
		model.addAttribute("cartItems",cart);
	}
	
	//Remove the item from cart
	public void removeItem(int id,Model model) {
		Optional<Sales> obj = repo.findById(id);
		for(int i=0;i<cart.size();i++) {
			if(cart.get(i).getId()==(obj.get().getId())) {
				cart.remove(i);
			}
		}
	}
	
	//Delete all items into the cart
	public void emptyCart() {
		cart.clear();
	}
}
