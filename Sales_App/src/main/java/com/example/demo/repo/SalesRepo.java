package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.Sales;

@Repository
public interface SalesRepo extends JpaRepository<Sales,Integer>{

}